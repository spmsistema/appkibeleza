import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Http, Headers, Response} from '@angular/http';
import { map } from 'rxjs/operators';
import { UrlService } from '../../servidor/url.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  nome: any;
  email: any;
  senha: any;
  cadastro: any;

  constructor(
    public formConst: FormBuilder,
    public http: Http,
    public nav: NavController,
    public servidorUrl: UrlService) {

      this.cadastro = this.formConst.group({
        nome: ['', Validators.required],
        email: ['', Validators.required],
        senha: ['', Validators.required],
      });
     }

     async cadCliente(){
        if(this.nome == undefined || this.email == undefined || this.senha == undefined){
          this.servidorUrl.alertas('Atenção', 'Preencha todos os campos');
        }else{

          this.cadDados(this.cadastro.value).subscribe(
            data => {
              console.log("Dados teste");
              this.servidorUrl.alertas('KiBeleza', this.nome + ', seu cadastro foi realizado com sucesso');
            },
            err => {
              console.log("Erro dados");
              this.servidorUrl.alertas('KiBeleza', this.nome + ', Erro as tentar realizar o cadastro');
            }
          );
        }
     }

     cadDados(nome){
      let cabecalho = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
      return this.http.post(this.servidorUrl.getUrl()+'cadastro.php', nome, {
        headers: cabecalho,
        method: 'POST'
      }).pipe(map(
        (res: Response) => { return res.json(); }
      ));
     }

     ngOnInit() {
    }

}
