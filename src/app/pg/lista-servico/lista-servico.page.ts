import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, Headers, Response } from '@angular/http';
import { UrlService } from '../../servidor/url.service';

@Component({
  selector: 'app-lista-servico',
  templateUrl: './lista-servico.page.html',
  styleUrls: ['./lista-servico.page.scss'],
})
export class ListaServicoPage implements OnInit {

  servicos: any;
  codigo: any;
  item: any;

  constructor(public http: Http, public servidorUrl: UrlService) {
    this.listaServico();
   }

   listaServico(){
      this.http.get(this.servidorUrl.getUrl()+"lista-servico.php").pipe(map(res => res.json()))
      .subscribe(
        listaDados => {
          this.servicos = listaDados;
        }
      )
   }

  ngOnInit() {
  }

}
