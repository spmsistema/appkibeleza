import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { Http } from '@angular/http';
import { UrlService } from '../../servidor/url.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email:any;
  senha:any;


  constructor(public alerta: AlertController,
              public http: Http,
              public urlServe: UrlService,
              public nav: NavController,
              public carregar: LoadingController ) {
                this.email = 'alle@alle.com.br';
                this.senha = 123;
              }

  async logar(){
    if(this.email == undefined || this.senha == undefined){

      this.urlServe.alertas('Atenção', 'Preencha todos os campos');

    }else{

      const loading = await this.carregar.create({
        spinner: "circles",
        message: 'Verificando login...',
      });
      
      await loading.present();

      this.http.get(this.urlServe.getUrl()+'login.php?email='+this.email+'&senha='+this.senha)
      .pipe(map(res => res.json()))
      .subscribe(data => {
        
        if(data.msg.logado == "sim"){
            if(data.dados.status == "ativo"){
              loading.dismiss();
              this.nav.navigateBack('/tabs/pg/home');

            } else {
              loading.dismiss();
              this.urlServe.alertas('Atenção', 'Usuário bloqueado!');
            }
        } else {
          loading.dismiss();
          this.urlServe.alertas('Atenção', 'Usuário ou senha invalido!');
        }
      });
    }
  }

  ngOnInit() {
  }

}
