import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { IonicModule } from '@ionic/angular';

import { ServicoPage } from './servico.page';
import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    component: ServicoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    HttpModule
  ],
  declarations: [ServicoPage]
})
export class ServicoPageModule {}
