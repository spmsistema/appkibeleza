import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { UrlService } from '../../servidor/url.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-servico',
  templateUrl: './servico.page.html',
  styleUrls: ['./servico.page.scss'],
})
export class ServicoPage implements OnInit {

id: any; 
detalhe: any;
dados: Array<{codigo:any, nome:any, valor:any, status:any, dataCadastro:any, foto1:any, foto2:any, foto3:any, texto:any, descricao:any, tempo:any, empresa:any}>;

  constructor(public http: Http, public servidorUrl: UrlService, public rota: ActivatedRoute) {

    this.rota.params.subscribe(parametroId => {
      this.id = parametroId.id;
      this.detalheServico();
      this.dados = [];
    });  
   }

   detalheServico(){
     this.http.get(this.servidorUrl.getUrl()+"detalhe-servico.php?idservico="+this.id).pipe(map(res => res.json()))
     .subscribe(
       data => {
         this.detalhe = data;

         for(let i = 0; i < data.length; i++){
            this.dados.push({
              codigo: data[i]['codigo'],
              nome: data[i]['nome'],
              valor: data[i]['valor'],
              status: data[i]['status'],
              dataCadastro: data[i]['dataCadastro'],
              foto1: data[i]['foto1'],
              foto2: data[i]['foto2'],
              foto3: data[i]['foto3'],
              descricao: data[i]['descricao'],
              texto: data[i]['texto'],
              tempo: data[i]['tempo'],
              empresa: data[i]['empresa']
            });
         }

         console.log(this.dados);
         console.log(this.dados[0].nome);
        }
     );
   }


  ngOnInit() { }

}
