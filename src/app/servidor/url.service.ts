import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})


export class UrlService {

  url: string = "http://allepalmeira.com.br/ti03-kibeleza/admin/";
  // url: string = "http://localhost:8080/ti03Kibeleza/admin/";

  constructor(public alerta: AlertController) { }

  getUrl(){
    return this.url;
  }

  async alertas(titulo, msg){
    const alert = await this.alerta.create({
      header: titulo,
      message: msg,
      buttons: ['OK']
    });
    await alert.present();
  }

}
